## [3.11.5](https://gitlab.com/to-be-continuous/semantic-release/compare/3.11.4...3.11.5) (2024-11-17)


### Bug Fixes

* add preset to semantic-release-info ([6c074f6](https://gitlab.com/to-be-continuous/semantic-release/commit/6c074f671b3e48480f8aea04f71df00f93d54fc0))

## [3.11.4](https://gitlab.com/to-be-continuous/semantic-release/compare/3.11.3...3.11.4) (2024-11-08)


### Bug Fixes

* fix eval_secret function when using node to decode a secret ([b579199](https://gitlab.com/to-be-continuous/semantic-release/commit/b579199b53dc58b0f94681720e245163ba457bc7)), closes [#49](https://gitlab.com/to-be-continuous/semantic-release/issues/49)

## [3.11.3](https://gitlab.com/to-be-continuous/semantic-release/compare/3.11.2...3.11.3) (2024-09-29)


### Bug Fixes

* make gpg configuration to work ([97d56ed](https://gitlab.com/to-be-continuous/semantic-release/commit/97d56edde8c378ae4eefdc52c7171a11fcd555e8)), closes [#48](https://gitlab.com/to-be-continuous/semantic-release/issues/48)

## [3.11.2](https://gitlab.com/to-be-continuous/semantic-release/compare/3.11.1...3.11.2) (2024-07-25)


### Bug Fixes

* trace without package.json ([3d7b6b0](https://gitlab.com/to-be-continuous/semantic-release/commit/3d7b6b01637b8fc9c2ba5aa3a295460f07c84fc8))

## [3.11.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.11.0...3.11.1) (2024-07-09)


### Bug Fixes

* generated (YAML) configuration syntax error when using JSON input config ([73865b9](https://gitlab.com/to-be-continuous/semantic-release/commit/73865b9e3bdd581eaf52f368cfd917fbe16473e4))

# [3.11.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.10.3...3.11.0) (2024-06-02)


### Features

* add support for configuring a commit specification (e.g. "conventional commits") ([c4519fd](https://gitlab.com/to-be-continuous/semantic-release/commit/c4519fdb1100437dced3ba2bf93c8c069545f303))

## [3.10.3](https://gitlab.com/to-be-continuous/semantic-release/compare/3.10.2...3.10.3) (2024-06-02)


### Bug Fixes

* npm cache and add yq cache ([1fc7b1a](https://gitlab.com/to-be-continuous/semantic-release/commit/1fc7b1ac6460c72e53b386b456997a9aa84ece35))

## [3.10.2](https://gitlab.com/to-be-continuous/semantic-release/compare/3.10.1...3.10.2) (2024-06-02)


### Bug Fixes

* provide a node-based implementation to retrieve url secrets ([f4e5a22](https://gitlab.com/to-be-continuous/semantic-release/commit/f4e5a225aa9816db907dc432cd842d86918ae838)), closes [#43](https://gitlab.com/to-be-continuous/semantic-release/issues/43)

## [3.10.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.10.0...3.10.1) (2024-05-28)


### Bug Fixes

* install gpg ([9533525](https://gitlab.com/to-be-continuous/semantic-release/commit/9533525aa8143bf93e84e41aaf4aed82ab01db15))

# [3.10.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.9.1...3.10.0) (2024-05-20)


### Features

* use slim image by default (shortens job execution time)  ([8dbd5da](https://gitlab.com/to-be-continuous/semantic-release/commit/8dbd5da4b489492523e9c678fa1c2b19dcd65283))

## [3.9.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.9.0...3.9.1) (2024-05-18)


### Bug Fixes

* semantic-release don't run on prod_ref ([49217b4](https://gitlab.com/to-be-continuous/semantic-release/commit/49217b4dfcfe2cb6f5bea72cce10cb2af2a95d59))

# [3.9.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.8.3...3.9.0) (2024-05-10)


### Features

* add SEMREL_REF to run semantic-release on a wider number of branches alongside info-on 'semrel' to use this variable ([74288af](https://gitlab.com/to-be-continuous/semantic-release/commit/74288afbe3fb22ec8c973b228755243b012f998a))

## [3.8.3](https://gitlab.com/to-be-continuous/semantic-release/compare/3.8.2...3.8.3) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([0bdec1c](https://gitlab.com/to-be-continuous/semantic-release/commit/0bdec1c625b01ad7b96629f5d448583fab9a1969))

## [3.8.2](https://gitlab.com/to-be-continuous/semantic-release/compare/3.8.1...3.8.2) (2024-04-03)


### Bug Fixes

* **vault:** use vault-secrets-provider's "latest" image tag ([7b3902d](https://gitlab.com/to-be-continuous/semantic-release/commit/7b3902db57fec34c4a46b500c19d6ba90d6bff42))

## [3.8.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.8.0...3.8.1) (2024-04-02)


### Bug Fixes

* allow semantic-release-info to be generated on the commit branch ([7876e3e](https://gitlab.com/to-be-continuous/semantic-release/commit/7876e3e7dab07671c87a65c325f1a86ed71cdbf5))

# [3.8.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.7.1...3.8.0) (2024-1-30)


### Bug Fixes

* <SEMREL_RELEASE_DISABLED/release-disabled> type ([02b6dd4](https://gitlab.com/to-be-continuous/semantic-release/commit/02b6dd4b654e32effc5ea9f3f8f774f2a82b38a9))
* dry run option ([6b05408](https://gitlab.com/to-be-continuous/semantic-release/commit/6b05408e85f635f350a0d2651ea0cff1dadd91ba))
* sanitize variable substitution pattern ([3ac15e7](https://gitlab.com/to-be-continuous/semantic-release/commit/3ac15e7c508a255a04e2f3df5f37eeab61e8340e))


### Features

* migrate to CI/CD component ([4ff7996](https://gitlab.com/to-be-continuous/semantic-release/commit/4ff7996643f34a6dd3e590b3c1f8cb6581dab042))

## [3.7.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.7.0...3.7.1) (2024-1-16)


### Bug Fixes

* **gpg:** create base folder if not present ([d69fae2](https://gitlab.com/to-be-continuous/semantic-release/commit/d69fae20f297d6e06333e8faac2c236fb824d37f)), closes [#32](https://gitlab.com/to-be-continuous/semantic-release/issues/32)

# [3.7.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.6.0...3.7.0) (2023-12-8)


### Features

* use centralized service images (gitlab.com) ([6b117cc](https://gitlab.com/to-be-continuous/semantic-release/commit/6b117cc0cca69deed7d8c62429482555673c8181))

# [3.6.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.5.2...3.6.0) (2023-11-02)


### Features

* add vault variant ([dd6fac6](https://gitlab.com/to-be-continuous/semantic-release/commit/dd6fac6c0faa6180302d6cc644d731ed2ee77a27))

## [3.5.2](https://gitlab.com/to-be-continuous/semantic-release/compare/3.5.1...3.5.2) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([b8f10b5](https://gitlab.com/to-be-continuous/semantic-release/commit/b8f10b5e18e4d998e81d5a84fc16afa9a8406c8c))

## [3.5.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.5.0...3.5.1) (2023-09-19)


### Bug Fixes

* always install extra plugins ([f011acd](https://gitlab.com/to-be-continuous/semantic-release/commit/f011acd9b92d67cb73ecc4612d9afb921f5258fb)), closes [#29](https://gitlab.com/to-be-continuous/semantic-release/issues/29)

# [3.5.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.4.2...3.5.0) (2023-08-04)


### Features

* make commit message configurable ([fbc63aa](https://gitlab.com/to-be-continuous/semantic-release/commit/fbc63aa9ed00c6714157cef53e3173433dd641a9))

## [3.4.2](https://gitlab.com/to-be-continuous/semantic-release/compare/3.4.1...3.4.2) (2023-07-13)


### Bug Fixes

* activate debug when TRACE is set ([fc3668b](https://gitlab.com/to-be-continuous/semantic-release/commit/fc3668b63b562e607418ba0023e5df34450dcfc5))

## [3.4.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.4.0...3.4.1) (2023-07-13)


### Bug Fixes

* make install_yq quiet ([61b7413](https://gitlab.com/to-be-continuous/semantic-release/commit/61b74134ce39b9e438f3999d22bc0db7f4f018bb))

# [3.4.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.3.1...3.4.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([40c252e](https://gitlab.com/to-be-continuous/semantic-release/commit/40c252e62fd954370d654572d753294f28122935))

## [3.3.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.3.0...3.3.1) (2023-03-27)


### Bug Fixes

* fix a regression in semantic-release-info job ([1b79f5d](https://gitlab.com/to-be-continuous/semantic-release/commit/1b79f5dfe6c2ae4ba29e73a18d6bcd5c09688da8))

# [3.3.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.2.2...3.3.0) (2023-03-21)


### Features

* allow to specify sem-rel and plugins version ([6b38d5b](https://gitlab.com/to-be-continuous/semantic-release/commit/6b38d5bc48157b8c6aef8f847a57b75faaf693dc))

## [3.2.2](https://gitlab.com/to-be-continuous/semantic-release/compare/3.2.1...3.2.2) (2023-02-27)


### Bug Fixes

* mark current directory as safe for git ([8f7e0be](https://gitlab.com/to-be-continuous/semantic-release/commit/8f7e0be5bbd7870515f7144d37b6ad4f9201ee7e))

## [3.2.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.2.0...3.2.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([81c4144](https://gitlab.com/to-be-continuous/semantic-release/commit/81c4144b2acfebff812c8e75a4fa430316977ce8))

# [3.2.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.1.1...3.2.0) (2022-12-05)


### Features

* display computed semantic-release-info ([5a1a37b](https://gitlab.com/to-be-continuous/semantic-release/commit/5a1a37be5df577d249205b917072467af208c392))

## [3.1.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.1.0...3.1.1) (2022-11-26)


### Bug Fixes

* support custom CA certificates with npm ([bf79a7d](https://gitlab.com/to-be-continuous/semantic-release/commit/bf79a7df66f4585288f872477eb386294a372b12))

# [3.1.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.0.0...3.1.0) (2022-10-16)


### Features

* enable commit signing ([dd6129b](https://gitlab.com/to-be-continuous/semantic-release/commit/dd6129b6e5dc9c9fad269add6a22d49ab003f5d0))

# [3.0.0](https://gitlab.com/to-be-continuous/semantic-release/compare/2.3.1...3.0.0) (2022-08-05)


### Features

* make MR pipeline the default workflow ([6128ce6](https://gitlab.com/to-be-continuous/semantic-release/commit/6128ce69cb57f407db307a2b775e31dca1c1a403))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

## [2.3.1](https://gitlab.com/to-be-continuous/semantic-release/compare/2.3.0...2.3.1) (2022-06-07)


### Bug Fixes

* install yq only if not found in image ([23041f1](https://gitlab.com/to-be-continuous/semantic-release/commit/23041f1a463079954184b831b5b40bbef8530ddc))

# [2.3.0](https://gitlab.com/to-be-continuous/semantic-release/compare/2.2.6...2.3.0) (2022-05-01)


### Features

* configurable tracking image ([b18fd89](https://gitlab.com/to-be-continuous/semantic-release/commit/b18fd89e223f84723f44c04d5b364bb375d78f16))

## [2.2.6](https://gitlab.com/to-be-continuous/semantic-release/compare/2.2.5...2.2.6) (2022-04-08)


### Bug Fixes

* remove verifyconditions in semrel-info ([e2e7b8f](https://gitlab.com/to-be-continuous/semantic-release/commit/e2e7b8f9bb094f172351dc6886f82efe718e115b))

## [2.2.5](https://gitlab.com/to-be-continuous/semantic-release/compare/2.2.4...2.2.5) (2022-03-19)


### Bug Fixes

* generate .releaserc in yaml format ([6e377bd](https://gitlab.com/to-be-continuous/semantic-release/commit/6e377bda26b7e5e2ae10d3b4b4c7b51e74640896))

## [2.2.4](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/to-be-continuous/semantic-release/compare/2.2.3...2.2.4) (2022-03-15)


### Bug Fixes

* don't overwrite existing .releaserc conf in semrel-info ([11c64dd](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/to-be-continuous/semantic-release/commit/11c64ddc53f65c77067bcf97649c8e23d948b05a))

## [2.2.3](https://gitlab.com/to-be-continuous/semantic-release/compare/2.2.2...2.2.3) (2022-02-25)


### Bug Fixes

* semantic-release is manual non-blocking by default ([fc8efc8](https://gitlab.com/to-be-continuous/semantic-release/commit/fc8efc85cbf86686cad40100c6377880152ccb12))

## [2.2.2](https://gitlab.com/to-be-continuous/semantic-release/compare/2.2.1...2.2.2) (2022-02-14)


### Bug Fixes

* add main branch for configuration generation  ([eb82e43](https://gitlab.com/to-be-continuous/semantic-release/commit/eb82e43dde2a3fdc23c83eeb33071a88f9f2560b)), closes [#14](https://gitlab.com/to-be-continuous/semantic-release/issues/14)

## [2.2.1](https://gitlab.com/to-be-continuous/semantic-release/compare/2.2.0...2.2.1) (2022-01-08)


### Bug Fixes

* set semantic-release job to download no artifacts at all ([ceb1379](https://gitlab.com/to-be-continuous/semantic-release/commit/ceb137958da6acceb368b1f1b4ba56dae0c4eda1))

# [2.2.0](https://gitlab.com/to-be-continuous/semantic-release/compare/2.1.0...2.2.0) (2021-11-23)


### Features

* overridable semrel config dir ([6bd0e4d](https://gitlab.com/to-be-continuous/semantic-release/commit/6bd0e4df9190ed75689e966d90946b35d7fd44f5))

# [2.1.0](https://gitlab.com/to-be-continuous/semantic-release/compare/2.0.5...2.1.0) (2021-11-16)


### Features

* allow to install additional npm package ([561c332](https://gitlab.com/to-be-continuous/semantic-release/commit/561c332331f02ef965fb32b52556a182e90c7b0e))

## [2.0.5](https://gitlab.com/to-be-continuous/semantic-release/compare/2.0.4...2.0.5) (2021-11-15)


### Bug Fixes

* prevent loops ([4ec78ea](https://gitlab.com/to-be-continuous/semantic-release/commit/4ec78ead772e7b4f88bcc3fe22f3bf8e561b823c))

## [2.0.4](https://gitlab.com/to-be-continuous/semantic-release/compare/2.0.3...2.0.4) (2021-10-07)


### Bug Fixes

* use master or main for production env ([56d8527](https://gitlab.com/to-be-continuous/semantic-release/commit/56d8527d8be27f4e1335f64fd29e5697abe1da9d))

## [2.0.3](https://gitlab.com/to-be-continuous/semantic-release/compare/2.0.2...2.0.3) (2021-09-27)


### Bug Fixes

* handle absent release property in package.json ([a5c73a9](https://gitlab.com/to-be-continuous/semantic-release/commit/a5c73a9d51d35ff98768d336467574c7b260ba4b))

## [2.0.2](https://gitlab.com/to-be-continuous/semantic-release/compare/2.0.1...2.0.2) (2021-09-09)

### Bug Fixes

* add changelog ([1682e12](https://gitlab.com/to-be-continuous/semantic-release/commit/1682e1274ad2524ac6b0805f19799cd824ef7806))

## [2.0.1](https://gitlab.com/to-be-continuous/semantic-release/compare/2.0.0...2.0.1) (2021-09-09)

### Bug Fixes

* make plugins installation faster ([bab645e](https://gitlab.com/to-be-continuous/semantic-release/commit/bab645e0a731aa5318dd2569868eb4886bbde50b))

## [2.0.0](https://gitlab.com/to-be-continuous/semantic-release/compare/1.2.0...2.0.0) (2021-09-01)


* Merge branch '3-change-variable-behaviour' into 'master' ([97daa97](https://gitlab.com/to-be-continuous/semantic-release/commit/97daa97bbbf5f956f176474b293a41eae4c6031d)), closes [#3](https://gitlab.com/to-be-continuous/semantic-release/issues/3)


### Features

* Change boolean variable behaviour ([077e9c0](https://gitlab.com/to-be-continuous/semantic-release/commit/077e9c0a1f75ffeece8f57f9ae970863cca08744))


### BREAKING CHANGES

* Change boolean variable behaviour
* boolean variable now triggered on explicit 'true' value


## [1.2.0](https://gitlab.com/to-be-continuous/semantic-release/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([9b39127](https://gitlab.com/to-be-continuous/semantic-release/commit/9b391279128aa00d1ec1b6dc09fe8a6f82168bba))


## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/semantic-release/compare/1.0.1...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([fe5e9d6](https://gitlab.com/Orange-OpenSource/tbc/semantic-release/commit/fe5e9d68e380d6b41f0cffa95e1bff2bb77dc42e))


## [1.0.1](https://gitlab.com/Orange-OpenSource/tbc/semantic-release/compare/1.0.0...1.0.1) (2021-05-12)

### Bug Fixes

* **generated-releaserc:** use strict yaml to avoid unexpected syntax error ([e68c72b](https://gitlab.com/Orange-OpenSource/tbc/semantic-release/commit/e68c72b070b4a91be0e217863a077ec2e4450f04))


## 1.0.0 (2021-05-06)


### Features

* initial release ([c711a35](https://gitlab.com/Orange-OpenSource/tbc/semantic-release/commit/c711a35b9f634372c420ab8fb5bb5cfaf87d384b))
